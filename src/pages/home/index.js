import React, { useEffect, useState } from 'react'
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  CheckBox,
  Picker,
  FlatList,
  Dimensions,
  Platform,
  SafeAreaView 

} from 'react-native';

import { Container, Header, Content, Tab, Tabs, Text, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import api from '../../services/api';


export default function home({ navigation }) {
  const [user, setUser] = useState(null);
  const [salas, setSalas] = useState(null);
  const [gyms, setGyms] = useState(null);
  const [minhassalas, setMinhasSalas] = useState(null);
  const getToken = async () => {
    const userToken = await AsyncStorage.getItem('token');
    const userId = await AsyncStorage.getItem('userId');

    api.get(`/users/${userId}`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setUser(response.data)
      })


    api.get(`/matchs/trat/`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setSalas(response.data)
      })

      api.get(`/matchs/responsible/${userId}`, {
        headers: {
          'Authorization': `Bearer ` + userToken
        }
      })
        .then(response => {
          setMinhasSalas(response.data)
          console.log(response.data)
        })
    
      api.get(`/gyms/all/`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setGyms(response.data)
      })




  }


  const entraSala = async (id) => {

    AsyncStorage.setItem("matchId", id.toString());

    navigation.navigate("salaPer");



  }


  useEffect(() => {


    if (!user && !salas) {
      getToken();
    }


  });
  const entraSalas = async (id) => {

    AsyncStorage.setItem("matchId", id.toString());

    navigation.navigate("sala");



  }


  useEffect(() => {


    if (!user && !salas) {
      getToken();
    }


  });



  return (




    <Container style={{ flex: 1 }}>
      <View style={{ alignItems: 'center', backgroundColor: '#C1C1C1', height: '27%' }}>

        <Image
          style={styles.image}
          source={require('../../assets/perfil.jpg')}
        />
      </View>

      <View style={{ backgroundColor: '#C1C1C1', flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>

        <View style={{marginRight: 55}}>

          <Text style={{ color: 'black', fontSize: 30 }}>
            {user && user[0].username}
          </Text>
        </View>

        <View>
        <Button onPress={() => navigation.navigate("criaSala")} style={{ width: 100, borderRadius: 2 }}>
          <Text style={{ textAlign: 'center'}}>
            Criar partida
          </Text>
        </Button>
        </View>
      </View>


      <Tabs initialPage={0} >
        <Tab heading="Minhas Salas" style={{ backgroundColor: '#C1C1C1' }} >
          <View style={[styles.container, { backgroundColor: '#345D7E', width:412}]}>
          <SafeAreaView style={{ height: 200}}>
          <FlatList
            data={minhassalas}
            keyExtractor={item => item.id}
            numColumns={1}
            renderItem={({ item }) => {
              return (
                <View style={{ maxWidth: 400 }}>
                  <Button onPress={() => entraSalas(item.id)} style={{ margin: 3, flexDirection: 'column', height: 30, width: 250, borderRadius: 10 }}>
                    <Text style={{ fontSize: 12, height: 30 }}>{item.name}</Text>
                  </Button>
                </View>
              );
            }}
          />
        </SafeAreaView>
          </View>
        </Tab>
        <Tab heading="Quadras" style={{ backgroundColor: '#C1C1C1'}}>
          <View style={[styles.container, { backgroundColor: '#345D7E', width:412}]}>
          <SafeAreaView style={{ height: 200}}>
          <FlatList
            data={gyms}
            keyExtractor={item => item.id}
            numColumns={1}
            renderItem={({ item }) => {
              return (
                <View style={{ maxWidth: 400 }}>
                  <Button style={{ margin: 3, flexDirection: 'column', height: 30, width: 250, borderRadius: 10 }}>
                    <Text style={{ fontSize: 12, height: 30 }}>{item.name}</Text>
                  </Button>
                </View>
              );
            }}
          />
        </SafeAreaView>
         
          </View>
        </Tab>
        <Tab heading="Parças" style={{ backgroundColor: '#C1C1C1'  }}>
          <View style={[styles.container, { backgroundColor: '#345D7E', width:412}]}>
            <Text>Faça novos amigos</Text>
          </View>
        </Tab>
      </Tabs>
      <View style={{ height: '30%', backgroundColor: '#C1C1C1', alignItems: 'center' }}>

        <Text style={{ fontSize: 20, alignItems: 'center' }}>Sugestões</Text>
        <SafeAreaView>
          <FlatList
            data={salas}
            keyExtractor={item => item.id}
            numColumns={3}
            renderItem={({ item }) => {
              return (
                <View style={{ maxWidth: 130 }}>
                  <Button onPress={() => entraSala(item.id)} style={{ margin: 3, flexDirection: 'column', height: 130, borderRadius: 10 }}>
                    <Text style={{ fontSize: 12, height: 30 }}>{item.name}</Text>
                    <Text style={{ fontSize: 12, height: 10 }}>--------------------------</Text>
                    <Text style={{ fontSize: 11 }}>Ginasio {item.gyms}</Text>
                    <Text style={{ fontSize: 9 }}>   ---    </Text>
                    <Text style={{ fontSize: 12 }}>Resp. {item.responsible}</Text>
                    <Text style={{ fontSize: 9 }}>{item.dataPart} {item.horarioPart}</Text>
                    <Text style={{ fontSize: 9 }}>               </Text>
                  </Button>
                </View>
              );
            }}
          />
        </SafeAreaView>

      </View>


    </Container>

  )
}


