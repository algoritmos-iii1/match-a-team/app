import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
    TextInput,
} from 'react-native';

export default styles = StyleSheet.create({
    background: {
       // flex: 1,
       // alignItems: 'center',
       // justifyContent: 'center',
        backgroundColor: 'black',
    },
     containerLogo: {
         flex: 1,
         justifyContent: 'center',

     },
    image: {
        width: 250,
        height: 300,
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },

    input: {
        backgroundColor: '#FFF',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10,
    },

    view: {
        width: '100%',
        marginBottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    btnSubmit: {
        backgroundColor: '#00BFFF',
        width: '90%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        marginTop: 10
    },
    
    combotext: {
        width:50, 
        height:50, 
        color: 'white', 
        fontSize:16, 
        marginTop: 20,
    },

    viewtt: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },

    combo: { 
        backgroundColor: '#00BFFF',
        width: '30%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        marginTop: 10
    },

    btnSubmitText: {
        color: '#FFF',
        fontSize: 18,
    },
    btnRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,

    },
    btnRegisterText: {
        color: 'white'
    },
    btnErroText: {
        color: 'red'
    }

});

