import React, { useEffect, useState } from 'react'
import {
  StyleSheet,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  CheckBox,
  Picker,
  FlatList,
  Dimensions,
  Platform,
  SafeAreaView,
  Alert

} from 'react-native';

import { Container, Header, Content, Tab, Tabs, Text, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import api from '../../services/api';


export default function home({ navigation }) {
  const [match, setMatch] = useState(null);
  const [salas, setSalas] = useState(null);
  const [gyms, setGyms] = useState(null);
  const [jogadores, setJogadores] = useState(null);
  const getToken = async () => {
    const userToken = await AsyncStorage.getItem('token');
    const userId = await AsyncStorage.getItem('userId');

    const matchId = await AsyncStorage.getItem('matchId');



    api.get(`/matchs/trata/${matchId}`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setMatch(response.data)
      })

    try {

      console.log("FIM " + matchId)
      api.get(`/matchs/findmatch/${matchId}`, {
        headers: {
          'Authorization': `Bearer ` + userToken
        }
      })
        .then(response => {
          setJogadores(response.data)
        })

    } catch (error) {
      console.log(error)
    }


    api.get(`/matchs/trat/`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setSalas(response.data)
      })

    api.get(`/gyms/all/`, {
      headers: {
        'Authorization': `Bearer ` + userToken
      }
    })
      .then(response => {
        setGyms(response.data)
      })




  }


  const entra = async () => {
    try {
      const userId = await AsyncStorage.getItem('userId')
      const userToken = await AsyncStorage.getItem('token');
      const matchId = await AsyncStorage.getItem('matchId');


      const body = {
          Matchs_id: matchId,
          users_id: userId,
      }

      api.post('matchs/inmatch', body, {
          headers: {
              'Authorization': `Bearer ` + userToken
          }

      }).then(response => console.log(response.data))

      navigation.navigate("home");
  } catch (error) {
      console.log(error)
  }


}


  const alertv1 = () =>
      Alert.alert(
        "Sair da partida",
        "Deseja sair deste jogo?",
        [
          {
            text: "Não",
            onPress: () => console.log("Cancel Pressed"),
          },
          { text: "Sim", onPress: () => entra() }
        ],
        { cancelable: false }
      );


  useEffect(() => {


    if (!match) {
      getToken();
    }

  });



  return (




    <Container style={{ flex: 2 }}>

      <View style={{ backgroundColor: '#C1C1C1', flexDirection: 'row', alignItems: 'center' }}>

        <View style={{ marginRight: 55 }}>

          <Text style={{ color: 'black', fontSize: 15 }}>
            Nome: {match && match[0].name}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Descrição: {match && match[0].description}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Responsavel: {match && match[0].responsible}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Dia do jogo: {match && match[0].dataPart}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Hora: {match && match[0].horarioPart}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Tempo de jogo: {match && match[0].tempoJogo}
          </Text>
          <Text style={{ color: 'black', fontSize: 15 }}>
            Local: {match && match[0].gyms}
          </Text>
        </View>
        <View>
        <Button onPress={() => alertv1()} style={{ width: 100, borderRadius: 10 }}>
          <Text style={{ textAlign: 'center'}}>
            Sair da partida
          </Text>
        </Button>
        </View>


      </View>
      <View style={{ backgroundColor: 'black', alignItems: 'center', width: 420 }}>
        <Text style={{ color: 'white', fontSize: 23 }}>
          Jogadores:
          </Text>
      </View>

      <View style={[styles.container, { backgroundColor: '#345D7E', width: 412 }]}>
        <SafeAreaView style={{ height: 200 }}>
          <FlatList
            data={jogadores}
            keyExtractor={item => item.Matchs_id}
            numColumns={1}
            renderItem={({ item }) => {
              return (
                <View style={{ maxWidth: 400 }}>
                  <Button style={{ margin: 3, flexDirection: 'column', height: 30, width: 250, borderRadius: 10 }}>
                    <Text style={{ fontSize: 12, height: 30 }}>{item.username}</Text>
                  </Button>
                </View>
              );
            }}
          />
        </SafeAreaView>

      </View>



    </Container>

  )
}


