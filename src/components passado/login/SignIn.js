import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    View,
    Image,
} from 'react-native';

import styled from 'styled-components';
import { Container } from '../../styled/Containers';
import StyledText from '../../styled/StyledText';
import TouchableCross from '../TouchableCross';
import StyledButton from '../../styled/StyledButton';
import StyledInput from '../../styled/StyledInput';
import Login from '../../services/api/Login'

const StyledImage = styled(Image)`
    width: 30;
    height: 35;
`;

export default function SignIn({navigation}) {
   
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    const doLogin = async () => {
        const login = new Login();

        const payload = { 
            email,
            password
        };

        try{
            const {data} = await login.access(payload);
            console.log('logou')
            // await AsyncStorage.setItem('token', data.token)
            // navigation.navigate('tuaRota');
        }catch(err){
            console.error(err);
        }
    };

    return (
        <Container>

            <StyledInput
                placeholder="Email"
                autoCorrect={false}
                onChangeText={(e) => { setEmail(e) }}
            />
            <StyledInput
                placeholder="Senha"
                autoCorrect={false}
                onChangeText={(e) => { setPassword(e) }}
            />

            <TouchableCross  onPress={() => doLogin()} >
                <StyledButton  bgColor={'green'}>
                    <StyledText color={'pink'} bold>Acessar </StyledText>
                </StyledButton>
            </TouchableCross>

            <TouchableCross>
                <StyledButton>
                    <StyledText color={'green'}>Criar Conta</StyledText>
                </StyledButton>
            </TouchableCross>

        </Container>

    )
}


const styles = StyleSheet.create({
    background: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black',
    },
    // containerLogo: {
    //     flex: 1,
    //     justifyContent: 'center',
    // },
    image: {
        width: 300,
        height: 350,

    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '90%',
        paddingBottom: 50
    },

    input: {
        backgroundColor: '#FFF',
        width: '90%',
        marginBottom: 15,
        color: '#222',
        fontSize: 17,
        borderRadius: 7,
        padding: 10,
    },
    btnSubmit: {
        backgroundColor: '#00BFFF',
        width: '90%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7
    },
    btnSubmitText: {
        color: '#FFF',
        fontSize: 18,
    },
    btnRegister: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 5,

    },
    btnRegisterText: {
        color: '#FFF'
    }

});
