import styled from 'styled-components'
import { TextInput } from 'react-native'


const StyledInput = styled(TextInput)`
    border-color: ${({ borderColor }) => borderColor};
    border-radius: ${ ({ borderRadius }) => borderRadius};
    background-color: ${ ({ bgColor }) => bgColor};
    color: ${ ({ color }) => color};
    padding-vertical: ${ ({ paddingVertical }) => paddingVertical};
    padding-horizontal: ${ ({ paddingHorizontal }) => paddingHorizontal};
    margin-vertical: ${ ({ marginVertical }) => marginVertical};
    margin-horizontal: ${ ({ marginHorizontal }) => marginHorizontal};
    width: ${ ({ width }) => width};
    font-size: ${ ({ fontSize }) => fontSize};
    margin-top: ${ ({ marginTop }) => marginTop};
    margin-bottom: ${ ({ marginBottom }) => marginBottom};
`
StyledInput.defaultProps = {
    fontSize: 16,
    borderWidth: 2,
    borderColor: '#1766E3',
    borderRadius: 25,
    paddingVertical: 14,
    paddingHorizontal: 20,
    marginVertical: 10,
    marginHorizontal: 0,
    bgColor: 'transparent',
    color: '#1766E3',
    width: '100%',
    marginTop: 0,
    marginBottom: 0,
}

export default StyledInput 
